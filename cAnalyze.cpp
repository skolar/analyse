#include "cAnalyze.hpp"

double x[MAX_DATA_SIZE];

void cAnalyze::analyze() {
	FILE *f;

	if( (f=fopen(filename, "r")) == NULL)
	{
		printf("soubor nenalezen: %s\n", filename);
		printline(lsize);
		return;
	};

	n=count_lines(f);
	rewind(f);
	int i;
	for(i=0; i<n; i++) {
		if(i==MAX_DATA_SIZE) {
			puts("překročili jste maximální kapacitu dat");
			n=--i;
			break;
		}
		fgets(s, SMAX, f);
		sscanf(s, "%lf\n", &x[i]);
		xp+=x[i];
		sig+=x[i]*x[i];
	}
	fclose(f);

	xp/=n;
	sig=sqrt((sig/n)-xp*xp);
	rel_sig=100*sig/xp;
	
}

void cAnalyze::out() {
//	if(output_style == NULL) strcpy(output_style, "LaTeX");
	if(!strcmp(output_style, "normal"))
		return normal_out();
	if(stejne(output_style, "LaTeX") || strstr(output_style, "latex"))
		return LaTeX_out();
	puts("Neznámý výstup");
}

void cAnalyze::normal_out() {
	printf("soubor\t\t= %s\n", filename);
	if(sloupec)
		printf("sloupec\t\t= %d\n", sloupec);
	printf("měření\t\t= %d\n", n);
	printf("průměr\t\t= %lf\n", xp);
	printf("odchylka\t= %lf\n", sig);
	printf("rel. odch.\t= %.3f \%\n", rel_sig);
	printline(lsize);
}


void cAnalyze::LaTeX_out() {
	char formunit[SMAX]="";

	if (stejne(jednotka, ""))
		sprintf(formunit, "\\ \\rm{%s}", jednotka);

	printf("%% soubor = %s\n", filename);
	if(sloupec)
		printf("%% sloupec = %d \\\\ \n", sloupec);
	printf("n = %d \\\\ \n", n);
	printf("\\overline{%s} = %lf %s\\\\\n", velicina, xp, formunit);
	printf("\\sigma = %lf %s\\\\\n", sig, formunit);
	printf("v_%s = %.3f\\ \\%% \\\\ \n", velicina, rel_sig);
}

