#ifndef CANALYZE_H
#define CANALYZE_H

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "funkce.hpp"

const int		lsize=29,
						MAX_DATA_SIZE=1e9,
						SMAX=256;

class cAnalyze {
	public :
		int		sloupec=0;
		char	filename[SMAX],
					output_style[SMAX]="normal",
					jednotka[SMAX]="",
					velicina[SMAX]="x";
					;

		void	analyze();
		void	out();
		void	normal_out();
		void	LaTeX_out();

	private:
	int 		n;
	double	xp, sig, rel_sig;
	char		s[SMAX];
};

#endif
