/*
 * analyse.cpp
 *
 * Copyright 2016 Štěpán Kolář
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "analyse.hpp"




int main(int argc, char **argv) {
	if(argc<=1) {
		puts("nezadal jste název souboru");
		return 1;
		}

	cAnalyze globmer;

	printline(lsize);
	while(--argc>0) {
		int vypocet	= 1,	// povolí/zakáže výpočet dat
				soubor	= 1;	// povolí/zakáže čtení souboru
		*++argv;
		cAnalyze mereni;

		if(*argv[0]=='-') {
			vypocet	= 0;
			soubor	= -1;
			char c;
			while(*++argv[0])
				switch(*argv[0])
				{
					case 'h'	:	help();
											break;
					case 'l' 	:
					case 'L'	:
											strcpy(globmer.output_style, "LaTeX");
											break;
					case 'v'	: ver();
											break;
					case '-'	:
											if(strcmp(*argv, "-help\0")==0)
												help();
											//else if(!s)
											else
												puts("neplatný přepínač");
											goto ven;
											break;
					default		: puts("neplatný přepínač");
											break;
				}
			vypocet++;
			soubor++;
		}
		printf("%s\n", *argv);
ven:
		if(vypocet >0 && soubor>0) {
			mereni=globmer;
			strcpy(mereni.filename, *argv);
			mereni.analyze();
			mereni.out();
		}
		soubor=1;
	}
	return 0;
}
