#include "funkce.hpp"


void ver(){
	puts("version: "VERSION);
}


int count_lines(FILE *f) {
	int count = 0;
	char c;

	while((c=getc(f))!=EOF)
		if(c == '\n')
			count++;
	return count;
}


void printline(int size) {
	char line[size];
	int i;
	for(i=0; i<size; i++)
		line[i]='-';
	printf("%%%s\n", line);
}


//řešení problém se <string.h> (z neznámého důvodu obráceně true a false)
int stejne(char *s1, const char *s2) {
	int i, s=strlen(s1);

	if( s!=strlen(s2) ) return 0;
	for(i=0; i<s; i++);
			if(s1[i]!=s2[i]) return 0;
	return 1;
}


void help() {
	printf("%s [přepínač] soubor\n", CMD);
	printf(	"  -L -l\t"		"použije LaTeXový formát výstupu\n"
					"  -h\t"			"zobrazí nápovědu\n"
					"  -v\t"			"vypíše verzi\n"
					"\n");
}




