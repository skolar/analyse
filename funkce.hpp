/*
 * funkce.hpp
 *
 *  Created on: 5. 2. 2016
 *      Author: stepan
 */

#ifndef FUNKCE_HPP_
#define FUNKCE_HPP_

#include <stdio.h>
#include <string.h>

#define CMD 		"analyse"
#define NAME		"Analyse"
#define VERSION "1.0.1"


void ver();
int stejne(char *s1, const char *s2);
int count_lines(FILE *f);
void printline(int size);
void help();


#endif /* FUNKCE_HPP_ */
